<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    // menampilkan isi data cast
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('/cast/indexcast', compact('cast'));
    }

    // menampilkan form tambah cast
    public function create()
    {
        return view('/cast/tambahcast');
    }
    // Memasukkan data kedalam database
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('sukses', 'Data Berhasil Ditambahkan');
    }
    // untuk menampilkan detail data 
    public function show($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('/cast/detailcast', compact('cast'));
    }
    // untuk menampilkan detail data untuk edit 
    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('/cast/editcast', compact('cast'));
    }
    // Update data
    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $cast_id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast')->with('sukses', 'Data Berhasil Diubah');
    }
    // delete data
    public function destroy($cast_id)
    {
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('Delete', 'Data Berhasil Dihapus');
    }
}