@extends('layouts.master')

@section('title', 'Detail')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Detail Cast</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-9">
                <!-- general form elements -->
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Detail Cast id {{$cast->id}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="nama" value="{{$cast->nama}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="umur" class="col-sm-2 col-form-label">Umur</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="umur" value="{{$cast->umur}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="bio">Bio</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" name="bio"
                                        readonly>{{$cast->bio}}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                    <div class="card-footer">
                        <a href="/cast" class="btn btn-secondary"><i class="fa fa-reply"></i> Kembali ke
                            Tabel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection