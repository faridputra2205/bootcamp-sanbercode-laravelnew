@extends('layouts.master')

@section('title','CAST')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Cast</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <!-- Button tambah data -->
                <a href="/cast/create" class="btn btn-success"><i class="fas fa-plus"></i> Tambah Data</a>
                <!-- akhir button -->
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Cast</h3>
                    </div>
                    @if(session('sukses'))
                    <div class="alert alert-primary" role="alert">
                        {{session('sukses')}}
                    </div>
                    @endif
                    @if(session('Delete'))
                    <div class="alert alert-danger" role="alert">
                        {{session('Delete')}}
                    </div>
                    @endif
                    <!-- /.card-header -->
                    <div class="card-body">
                        <!-- tabel -->
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10px">No</th>
                                    <th>Nama</th>
                                    <th>Umur</th>
                                    <th>Bio</th>
                                    <th>Action</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($cast as $key=>$casts)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$casts->nama}}</td>
                                    <td>{{$casts->umur}}</td>
                                    <td>{{$casts->bio}}</td>
                                    <td style="width:16%;">
                                        <a href="/cast/{{$casts->id}}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-share-square"></i>
                                            Detail</a>
                                        <a href="/cast/{{$casts->id}}/edit" class="btn btn-primary btn-sm"><i
                                                class="fa fa-edit"></i> Edit</a>
                                    </td>
                                    <td>
                                        <form action="/cast/{{$casts->id}}" method="POST">
                                            {{csrf_field()}}
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr colspan="3">
                                    <td>No data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- akhir tabel -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
</section>

@endsection