<?php

use App\Http\Controller\HomeController;
use App\Http\Controller\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/Register', 'AuthController@register');
Route::post('/Welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('/layouts/master');
});
Route::get('/dashboard', function () {
    return view('dashboardadmin');
});
Route::get('/table', function () {
    return view('/tabel/tabel');
});
Route::get('/data-tables', function () {
    return view('/tabel/datatabel');
});

// Cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');